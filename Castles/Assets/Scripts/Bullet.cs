﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Assertions;

public class Bullet : BulletMove
{
    [SerializeField]
    Sprite[] m_bullet_sprites = null; //from the smallest to the largest
    [SerializeField]
    GameObject m_explosion = null;
    [SerializeField]
    GameObject m_hp_loss_text = null;
    WeaponInfo m_weapon_parent_info;
    bool m_shootinfo_not_added = true;

    public static Dictionary<eWeaponType, float> m_weapon_to_size = new Dictionary<eWeaponType, float>()
    {
    { eWeaponType.small, 0.3f}, 
    { eWeaponType.medium, 0.6f}, 
    { eWeaponType.large, 1f } 
    };
    void Awake()
    {
        m_weapon_parent_info = TurnManager.CurrentWeaponInfo();
        GetComponent<SpriteRenderer>().sprite = m_bullet_sprites[Weapon.WeaponType2SpriteIndex(m_weapon_parent_info.m_weapon_type)];
        Assert.AreNotEqual(0,m_weapon_parent_info.m_power);
    }
    void OnTriggerEnter2D(Collider2D a_colider)
    {
        if (m_shootinfo_not_added && TurnManager.m_player_turn == ePlayerTurn.computer)
        {
            m_shootinfo_not_added = false;
            ComputerWeaponControl.m_computer_angle_calculate.AddBulletShootInfo(new BulletDamageInfo(a_colider.transform.position, a_colider.tag.Equals("Castle") && a_colider.GetComponentInParent<CastleDamage>().m_owner != ePlayerTurn.computer));
        }

        if (a_colider.tag.Equals("Castle"))
        {
            CastleDamage castle_damage_instance = a_colider.transform.parent.gameObject.GetComponent<CastleDamage>();
            Assert.IsNotNull(castle_damage_instance, "castle doesnt have castleDamage script");
            int hp_loss = CastleDamage.TranslateHitPart2Damage(a_colider.transform.name);
            castle_damage_instance.m_HP -= hp_loss;
            GameObject text_loss = (GameObject)Instantiate(m_hp_loss_text, transform.position, Quaternion.identity);
            text_loss.GetComponentInChildren<HPLossText>().ChangeText(hp_loss);
            DestroyBullet();
        }
        else if (a_colider.tag.Equals("Obstacle"))
        {
            DestroyBullet();
        }
    }

    void OnDestroy()
    {
        GameObject explosion = (GameObject)Instantiate(m_explosion, transform.position, Quaternion.identity);
        ChangeExplosionScale(ref explosion);
        Destroy(explosion, 1f);
        if (TurnManager.SINGLE_MODE)
        {
            if (TurnManager.m_chopper_spawned)
            {
                TurnManager.m_player_turn = TurnManager.m_single_mode_player;
                return;
            }
            if (m_shootinfo_not_added && TurnManager.m_player_turn != ePlayerTurn.computer)
            {
                m_shootinfo_not_added = false;
                ComputerWeaponControl.m_computer_angle_calculate.AddBulletShootInfo(new BulletDamageInfo(transform.position, false));
            }
            TurnManager.m_player_turn = TurnManager.m_player_turn == TurnManager.m_single_mode_player ? ePlayerTurn.computer : TurnManager.m_single_mode_player; // chamge turn //single mode player which castle belongs to player = 0, computer
        }
        else
        {
            TurnManager.m_player_turn = TurnManager.m_player_turn == ePlayerTurn.first_player ? ePlayerTurn.second_player : ePlayerTurn.first_player; // chamge turn //first player = 0, second = 1
        }
    }

    /// <summary>
    /// Change explosion scale acording to bullet size
    /// </summary>
    /// <param name="explosion"></param>
    private void ChangeExplosionScale(ref GameObject explosion)
    {
        explosion.transform.Find("FireBall").GetComponent<ParticleSystem>().startSize *= m_weapon_to_size[m_weapon_parent_info.m_weapon_type];
        explosion.transform.Find("Shockwave").GetComponent<ParticleSystem>().startSize *= m_weapon_to_size[m_weapon_parent_info.m_weapon_type];
        explosion.transform.Find("BaseSmoke").GetComponent<ParticleSystem>().startSize *= m_weapon_to_size[m_weapon_parent_info.m_weapon_type];
    }

    private void DestroyBullet()
    {
        Destroy(transform.gameObject);
    }
}
