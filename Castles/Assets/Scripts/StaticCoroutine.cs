﻿using UnityEngine;
using System.Collections;
using System;

public class StaticCoroutine : MonoBehaviour
{
    public static StaticCoroutine instance = null;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    IEnumerator StaticCoroutineMethod(IEnumerator a_coroutine, Action onComplete = null)
    {
        onComplete = onComplete ?? delegate { };
        yield return StartCoroutine(a_coroutine);
        onComplete();
    }
    static public void DoCoroutine(IEnumerator coroutine, Action onComplete = null)
    {
        instance.StartCoroutine(instance.StaticCoroutineMethod(coroutine, onComplete)); //this will launch the coroutine on our instance
    }
}
