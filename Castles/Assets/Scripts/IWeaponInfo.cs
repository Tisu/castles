﻿using System;

interface IWeaponInfo
{
    WeaponInfo GetWeaponInfo();
}
