﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;

public class BulletMove : MonoBehaviour
{
    Rigidbody2D m_rigidbody = null;
    Vector2 m_spawn_position;
    private Vector2 m_velocity_vector = Vector2.zero;
    private float m_rotation_z = 0f;

    void Start()
    {
        m_spawn_position = transform.position;
        m_rigidbody = GetComponent<Rigidbody2D>();
        WeaponInfo weapon_info = TurnManager.CurrentWeaponInfo();
        Vector2 spawn_vector = ((Vector2)transform.position - (Vector2)weapon_info.m_position).normalized;       
        m_rigidbody.AddForce(weapon_info.m_power * spawn_vector, ForceMode2D.Impulse);
    }

    void FixedUpdate()
    {
        if ((Vector2)transform.position != m_spawn_position)
        {
            m_velocity_vector = m_rigidbody.GetPointVelocity(transform.position);
            m_rotation_z = Mathf.Atan2(m_velocity_vector.y, m_velocity_vector.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, transform.rotation.y, m_rotation_z));
        }
        Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y, Camera.main.transform.position.z);
        Camera.main.orthographicSize = 10;
    }  
}

