﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HPLossText: MonoBehaviour 
{
    Vector2 m_target_position = Vector2.zero;
    const float MAX_DELTA = 0.5f;
    void Awake()
    {
        Destroy(transform.gameObject, 1f);
        m_target_position = (Vector2)transform.position + new Vector2(0, 2f);
    }
    void FixedUpdate()
    {
        transform.position = Vector2.MoveTowards(transform.position,m_target_position,MAX_DELTA);
    }
    public void ChangeText(int a_hp_loss)
    {
        GetComponentInChildren<Text>().text = "-" + a_hp_loss;
    }
}
