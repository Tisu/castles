﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;
using UnityEngine.UI;

public enum ePlayerTurn
{
    first_player = 0,
    second_player = 1,
    game_over = 2,
    computer = 3,
}
public enum eMode
{
    single,
    two_players
}
public class TurnManager : MonoBehaviour
{
    public static bool SINGLE_MODE = true;
    public static bool GAME_OVER = false;
    public static ePlayerTurn m_lost_player;
    public static ePlayerTurn m_single_mode_player;
    public static Vector3[] CASTLES_MAIN_CAMERA_POSITION; //position of casles 0- castle1 1- castle2 !warning z is camera position.z
    public static ComputerWeaponControl m_computer_control_script = null;
    public static Weapon m_cannon_script_player1;
    public static Weapon m_cannon_script_player2;
    public static bool m_chopper_spawned = false;
    public static Chopper m_chopper_script_instance = null;

    private static ePlayerTurn m_player_turn_private;
    private static int m_computer_castle_index = 0;
    private static GameObject m_gameover_menu = null;
    [SerializeField]
    private GameObject m_game_over_menu_nonstatic = null;

    public static ePlayerTurn m_player_turn
    {
        get
        {
            return m_player_turn_private;
        }

        set
        {
            if (m_player_turn_private == ePlayerTurn.game_over)
            {
                return;
            }
            m_player_turn_private = value;
            switch (m_player_turn_private) // when turn switch proper player can shoot
            {
                case ePlayerTurn.first_player:
                    {
                        m_cannon_script_player1.m_can_shot = true;
                        m_cannon_script_player2.m_can_shot = false;
                        Camera.main.orthographicSize = 5;
                        StaticCoroutine.DoCoroutine(DelayCameraChange(0));
                        break;
                    }
                case ePlayerTurn.second_player:
                    {
                        m_cannon_script_player2.m_can_shot = true;
                        m_cannon_script_player1.m_can_shot = false;
                        Camera.main.orthographicSize = 5;
                        StaticCoroutine.DoCoroutine(DelayCameraChange(1));
                        break;
                    }
                case ePlayerTurn.game_over:
                    {
                        Camera.main.orthographicSize = 20;
                        Camera.main.transform.position = new Vector3(0, 0, Camera.main.transform.position.z);
                        m_cannon_script_player2.m_can_shot = false;
                        m_cannon_script_player1.m_can_shot = false;
                        GameOver();
                        break;
                    }
                case ePlayerTurn.computer:
                    {
                        m_cannon_script_player2.m_can_shot = false;
                        m_cannon_script_player1.m_can_shot = false;
                        Camera.main.orthographicSize = 5;
                        StaticCoroutine.DoCoroutine(DelayCameraChange(m_computer_castle_index));
                        m_computer_control_script.Shoot();
                        break;
                    }
                default:
                    {
                        Assert.IsFalse(true, "More than 2 player, not implemented");
                        break;
                    }
            }
        }
    }

    private static void GameOver()
    {
        string gameover_text;
        if (TurnManager.SINGLE_MODE)
        {
            if (m_lost_player != ePlayerTurn.computer)
            {
                gameover_text = "You've lost!";
            }
            else
            {
                gameover_text = "You've won!";

            }
        }
        else
        {
            gameover_text = "Player " + ((int)m_lost_player + 1) + " won!";
        }
        GameObject game_over_menu_instance = Instantiate(m_gameover_menu, new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, 0), Quaternion.identity) as GameObject;
        game_over_menu_instance.transform.Find("Canvas (1)").GetComponentInChildren<Text>().text = gameover_text;
        if (DataManager.m_current_save_data.m_current_weapon != eWeaponType.large)
        {
            game_over_menu_instance.transform.Find("Canvas (2)").GetComponentInChildren<Text>().text += DataManager.m_current_win_streak + " / " + CannonCost.m_cannon_cost[(eWeaponType)(DataManager.m_current_save_data.m_current_weapon + 1)];
        }
    }
    private static IEnumerator DelayCameraChange(int a_castle_index)
    {
        yield return new WaitForSeconds(1f);
        Camera.main.transform.position = CASTLES_MAIN_CAMERA_POSITION[a_castle_index];
    }
    public static WeaponInfo CurrentWeaponInfo()
    {
        switch (m_player_turn)
        {
            case ePlayerTurn.first_player:
                {
                    return m_cannon_script_player1.GetWeaponInfo();
                }
            case ePlayerTurn.second_player:
                {
                    return m_cannon_script_player2.GetWeaponInfo();
                }
            case ePlayerTurn.computer:
                {
                    return m_computer_control_script.GetWeaponInfo();
                }
            default:
                {
                    Assert.IsFalse(true, "not implemented,current turn: computer");
                    return new WeaponInfo();
                }
        }
    }
    void Start()
    {
        m_gameover_menu = m_game_over_menu_nonstatic;
#warning uncomment this
        //TurnManager.SINGLE_MODE = PlayerPrefs.GetInt("Mode") == 1 ? true : false;
        var castle1 = GameObject.Find("Castle 1");
        var castle2 = GameObject.Find("Castle 2");
        TurnManager.m_cannon_script_player1 = castle1.GetComponentInChildren<Weapon>();
        TurnManager.m_cannon_script_player2 = castle2.GetComponentInChildren<Weapon>();
        Vector3 castle_position1 = transform.TransformPoint(castle1.transform.Find("main_tower").transform.position);
        Vector3 castle_position2 = transform.TransformPoint(castle2.transform.Find("main_tower").transform.position);
        CASTLES_MAIN_CAMERA_POSITION = new Vector3[2]
        {
          new Vector3(castle_position1.x, castle_position1.y, Camera.main.transform.position.z),
          new Vector3(castle_position2.x, castle_position2.y, Camera.main.transform.position.z) 
        };

        TurnManager.m_player_turn = Random.Range(0, 2) == 0 ? ePlayerTurn.first_player : ePlayerTurn.second_player; //random never return max value
        if (TurnManager.SINGLE_MODE)
        {
            TurnManager.m_single_mode_player = TurnManager.m_player_turn;
            m_computer_castle_index = TurnManager.m_single_mode_player == ePlayerTurn.first_player ? 1 : 0; // oposite castle belongs to computer
        }
        switch (TurnManager.m_player_turn)
        {
            case ePlayerTurn.first_player:
                {
                    castle1.GetComponent<CastleDamage>().m_owner = ePlayerTurn.first_player;
                    if (TurnManager.SINGLE_MODE)
                    {
                        castle2.GetComponent<CastleDamage>().m_owner = ePlayerTurn.computer;
                    }
                    else
                    {
                        castle2.GetComponent<CastleDamage>().m_owner = ePlayerTurn.second_player;
                    }
                    break;
                }
            case ePlayerTurn.second_player:
                {
                    castle1.GetComponent<CastleDamage>().m_owner = ePlayerTurn.second_player;
                    if (TurnManager.SINGLE_MODE)
                    {
                        castle2.GetComponent<CastleDamage>().m_owner = ePlayerTurn.computer;
                    }
                    else
                    {
                        castle2.GetComponent<CastleDamage>().m_owner = ePlayerTurn.first_player;
                    }
                    break;
                }
            case ePlayerTurn.game_over:
                break;
            case ePlayerTurn.computer:
                break;
            default:
                break;
        }

        Assert.raiseExceptions = true;
        Assert.IsNotNull(TurnManager.m_cannon_script_player1, "player1 script is null");
        Assert.IsNotNull(TurnManager.m_cannon_script_player2, "player2 script is null");
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.LoadLevel(0);
        }
    }
}
