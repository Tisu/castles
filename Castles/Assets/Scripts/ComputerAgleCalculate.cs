﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public struct ShootInfoShort
{
    public float m_angle;
    public float m_power;
    public ShootInfoShort(float a_angle, float a_power)
    {
        m_angle = a_angle;
        m_power = a_power;
    }
}

public class ComputerAgleCalculate
{
    private struct ShootInfoFull
    {
        public BulletDamageInfo m_bullet_info;
        public float m_angle;
        public float m_power;
        public ShootInfoFull(BulletDamageInfo a_bullet_damage_info, float a_angle, float a_power)
        {
            m_bullet_info = a_bullet_damage_info;
            m_angle = a_angle;
            m_power = a_power;
        }
        public static bool operator ==(ShootInfoFull x, ShootInfoFull y)
        {
            return x.m_power == y.m_power
                && x.m_angle == y.m_angle
                && x.m_bullet_info.m_hit_position_difference == y.m_bullet_info.m_hit_position_difference
                && x.m_bullet_info.m_castle_hit == y.m_bullet_info.m_castle_hit;
        }
        public static bool operator !=(ShootInfoFull x, ShootInfoFull y)
        {
            return !(x == y);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
    }

    private enum eLastShootType
    {
        angle,
        power
    }

    public Queue<ShootInfoShort> m_last_shoot_info_shortQ = new Queue<ShootInfoShort>();
    public Vector3 m_enemy_castle_position { get; set; }
    public Vector3 m_computer_castle_position { get; set; }
    public Transform m_raycast_start = null;
    public float m_cannon_reference_level = 0f;

    Vector3 m_castle_vector = Vector3.zero;
    private Queue<ShootInfoFull> m_last_shoot_info_fullQ = new Queue<ShootInfoFull>();
    int m_missed_shoot = 0;
    bool m_force_angle_change = false;
    bool m_force_power_change = false;
    eLastShootType m_last_change = eLastShootType.angle;
    Queue<eLastShootType> m_last_forced_shoots;
    const float SECONDS_TO_CHANGE_ROTATION = 10f;
    const int PREDICT_BACKWARDS = 1;
    const int MAX_MISSED_SHOOT = 5;
    const float DISTANCE_THRESHOLD = 2.5f;

    private ShootInfoShort CalculateShoot()
    {
        if (m_last_shoot_info_fullQ.Count == 0)
        {
            m_castle_vector = m_enemy_castle_position - m_computer_castle_position;
            return RandomInfoShort();
        }
        ShootInfoFull[] target_hit = m_last_shoot_info_fullQ.Where(x => x.m_bullet_info.m_castle_hit == true).ToArray();
        if (target_hit.Length > 1)
        {
            if (m_missed_shoot >= MAX_MISSED_SHOOT) // finish him ;)
            {
                return new ShootInfoShort(target_hit.Last().m_angle, target_hit.Last().m_power);
            }
            int sign = Random.Range(0, 2) == 0 ? -1 : 1;
            switch (Random.Range(0, 3))
            {
                case 0:
                    {
                        return new ShootInfoShort(target_hit.Last().m_angle, target_hit.Last().m_power + Random.Range(1f, 15f) * sign);
                    }
                case 1:
                    {
                        return new ShootInfoShort(target_hit.Last().m_angle + Random.Range(1f, 8f) * sign, target_hit.Last().m_power);
                    }
                case 2:
                    {
                        return RandomInfoShort();
                    }
                default:
                    {
                        return RandomInfoShort();
                    }
            }
        }
        else if (m_last_shoot_info_fullQ.Count > 2
            && m_last_shoot_info_fullQ.Last() == m_last_shoot_info_fullQ.ElementAt(m_last_shoot_info_fullQ.Count - 2)) // if last one = second last should never happend just in case
        {
            m_last_shoot_info_fullQ.Dequeue();
            return RandomInfoShort();
        }
        else
        {
            return PredictedInfoShort();
        }
    }

    private ShootInfoShort PredictedInfoShort()
    {
        if (m_last_shoot_info_fullQ.Count == 1)
        {
            ShootInfoFull last_shoot_info = m_last_shoot_info_fullQ.Last();
            if (Mathf.Abs(last_shoot_info.m_bullet_info.m_hit_position_difference.x) > Mathf.Abs(last_shoot_info.m_bullet_info.m_hit_position_difference.y))
            {
                return new ShootInfoShort(Mathf.Abs(last_shoot_info.m_angle), last_shoot_info.m_power + Random.Range(0f, 0.5f) * last_shoot_info.m_bullet_info.m_hit_position_difference.x);
            }
            else
            {
                int sign = m_cannon_reference_level > 120 ? -1 : 1;
                return new ShootInfoShort(Mathf.Abs(last_shoot_info.m_angle) + Random.Range(0f, .2f) * last_shoot_info.m_bullet_info.m_hit_position_difference.y * sign, last_shoot_info.m_power);
            }
        }
        if (m_last_shoot_info_fullQ.Count > 2)
        {
            if (!CheckLastShoots()) // if forced 
            {
                return RandomInfoShort();
            }
        }

        ShootInfoFull[] info = m_last_shoot_info_fullQ.OrderBy(x => x.m_bullet_info.m_hit_position_difference.magnitude).ToArray();

        if (m_force_power_change || (!m_force_angle_change && Mathf.Abs(info[0].m_bullet_info.m_hit_position_difference.x) > Mathf.Abs(info[0].m_bullet_info.m_hit_position_difference.y))) // change power
        {
            float distance_difference = Mathf.Abs(info[1].m_bullet_info.m_hit_position_difference.x - info[0].m_bullet_info.m_hit_position_difference.x);
            if (distance_difference == 0f)
            {
                m_last_shoot_info_fullQ.Clear();
                return RandomInfoShort();
            }
            float power_per_unit = (info[1].m_power - info[0].m_power) / distance_difference;
            if (power_per_unit == 0f)
            {
                if (info[0].m_bullet_info.m_hit_position_difference.x > 0)
                {
                    power_per_unit = Random.Range(-1f, 0f);
                }
                else
                {
                    power_per_unit = Random.Range(0f, 1f);
                }
            }
            int sign = m_cannon_reference_level > 120f ? -1 : 1;
            float new_power = info[0].m_power + (info[0].m_bullet_info.m_hit_position_difference.x * power_per_unit * sign);
            m_last_change = eLastShootType.power;
            return new ShootInfoShort(info[0].m_angle, new_power);
        }
        else//change angle
        {
            float distance_difference = Mathf.Abs(info[1].m_bullet_info.m_hit_position_difference.y - info[0].m_bullet_info.m_hit_position_difference.y);
            if (distance_difference == 0f)
            {
                m_last_shoot_info_fullQ.Clear();
                return RandomInfoShort();
            }
            float angle_per_unit = (info[1].m_angle - info[0].m_angle) / distance_difference;
            if (angle_per_unit == 0f)
            {
                if (info[0].m_bullet_info.m_hit_position_difference.y > 0)
                {
                    angle_per_unit = Random.Range(-1f, 0f);
                }
                else
                {
                    angle_per_unit = Random.Range(0f, 1f);
                }
            }
            float new_angle = (info[0].m_angle + (info[0].m_bullet_info.m_hit_position_difference.y * angle_per_unit));
            m_last_change = eLastShootType.angle;
            return new ShootInfoShort(new_angle, info[0].m_power);
        }
    }

    /// <summary>
    /// flase if calculate random
    /// </summary>
    /// <returns></returns>
    private bool CheckLastShoots()
    {
        Vector3 last_shoot_position = m_last_shoot_info_fullQ.Last().m_bullet_info.m_hit_position_difference;
        Vector3 penultimate_shoot_position = m_last_shoot_info_fullQ.ElementAt(m_last_shoot_info_fullQ.Count - 2).m_bullet_info.m_hit_position_difference;
        float distance = Vector3.Distance(last_shoot_position, penultimate_shoot_position);
        switch (m_last_change)
        {
            case eLastShootType.angle:
                {
                    if (distance > DISTANCE_THRESHOLD)
                    {
                        if (m_last_forced_shoots.Count >= 2) // when two times forced in a row
                        {
                            m_last_forced_shoots.Clear();
                            return false;
                        }
                        if (m_last_forced_shoots.Count == 0)
                        {
                            m_last_forced_shoots.Enqueue(eLastShootType.angle);
                            m_force_angle_change = true;
                            m_force_power_change = false;
                            return true;
                        }
                        else // last shoot was angle and it must have benn forced count > 1
                        {
                            m_last_forced_shoots.Enqueue(eLastShootType.power);
                            m_force_angle_change = false;
                            m_force_power_change = true;
                            return true;
                        }
                    }
                    else // if not in threshold range
                    {
                        if (m_last_forced_shoots.Count > 0)
                        {
                            m_last_forced_shoots.Dequeue();
                        }
                        m_force_power_change = false;
                        m_force_angle_change = false;
                        return true;
                    }
                }
            case eLastShootType.power:
                {
                    if (distance > DISTANCE_THRESHOLD)
                    {
                        if (m_last_forced_shoots.Count >= 2) // when two times forced in a row
                        {
                            m_last_forced_shoots.Clear();
                            return false;
                        }
                        if (m_last_forced_shoots.Count == 0)
                        {
                            m_last_forced_shoots.Enqueue(eLastShootType.power);
                            m_force_power_change = true;
                            m_force_angle_change = false;
                            return true;
                        }
                        else  // last shoot was power and it must have benn forced count > 1
                        {
                            m_last_forced_shoots.Enqueue(eLastShootType.angle);
                            m_force_angle_change = true;
                            m_force_power_change = false;
                            return true;
                        }
                    }
                    else // if not in threshold range
                    {
                        if (m_last_forced_shoots.Count > 0)
                        {
                            m_last_forced_shoots.Dequeue();
                        }
                        m_force_power_change = false;
                        m_force_angle_change = false;
                        return true;
                    }
                }
            default:
                {
                    Debug.LogError("should never happen");
                    return true;
                }
        }
    }

    private ShootInfoShort RandomInfoShort()
    {
        float power = Random.Range(m_castle_vector.magnitude - 10f, 100f);
        Vector2 random_range = power > 80f ? new Vector2(5f, 20f) : new Vector2(20f, 50f);
        float angle = Mathf.Atan2(m_castle_vector.y, m_castle_vector.x) + (Random.Range(random_range.x, random_range.y));

        RaycastHit2D hit = Physics2D.Raycast((Vector2)m_raycast_start.position, Quaternion.Euler(0, 0, angle) * Vector2.up, m_castle_vector.magnitude);
        if (!hit)
        {
            int sign = m_cannon_reference_level > 120 ? -1 : 1;
            return new ShootInfoShort(angle * sign + m_cannon_reference_level, power);
        }
        else
        {
            if (!hit.transform.tag.Equals("Castle"))
            {
                int sign = m_cannon_reference_level > 120 ? -1 : 1;
                return new ShootInfoShort(angle * sign + m_cannon_reference_level, power);
            }
            RandomInfoShort();
        }
        return new ShootInfoShort(0, 0);
    }

    public void AddBulletShootInfo(BulletDamageInfo a_bullet_damage_info)
    {
        if (m_last_shoot_info_shortQ.Count < 1)
        {
            return;
        }
        if (m_last_shoot_info_fullQ.Count >= 10)
        {
            m_last_shoot_info_fullQ.Dequeue();
        }
        m_missed_shoot = a_bullet_damage_info.m_castle_hit == true ? 0 : ++m_missed_shoot;
        ShootInfoShort shot_info = m_last_shoot_info_shortQ.Dequeue();
        m_last_shoot_info_fullQ.Enqueue(new ShootInfoFull
            (new BulletDamageInfo(m_enemy_castle_position - a_bullet_damage_info.m_hit_position_difference, a_bullet_damage_info.m_castle_hit)
            , shot_info.m_angle, shot_info.m_power));
    }
    public void AddLastShootInfo(float a_angle, float a_power)
    {
        m_last_shoot_info_shortQ.Enqueue(new ShootInfoShort(a_angle, a_power));
    }
    public ShootInfoShort AdjustAngle(float a_cannon_eulerAngle_z, out float o_angle_change)
    {
        ShootInfoShort shoot_info = CalculateShoot();
        o_angle_change = (shoot_info.m_angle - a_cannon_eulerAngle_z) / SECONDS_TO_CHANGE_ROTATION;
        return shoot_info;
    }
}
