﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour
{
    [SerializeField]
    GameObject m_explosion;
    const int m_bomb_damage = 10;
    void OnTriggerEnter2D(Collider2D a_collider)
    {   
        Instantiate(m_explosion, transform.position, Quaternion.identity);
        if (a_collider.tag.Equals("Castle"))
        {
            a_collider.GetComponent<CastleDamage>().m_HP -= m_bomb_damage;
        }
        Destroy(gameObject);
    }
}
