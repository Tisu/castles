﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CastleDamage : MonoBehaviour
{
    public ePlayerTurn m_owner;
    public enum eCastlePartDamage
    {
        front = 20,
        main = 40,
        back = 10,
    }
    private int m_hp = 100;
    public int m_HP
    {
        get
        {
            return m_hp;
        }
        set
        {
            if (TurnManager.m_chopper_spawned && TurnManager.m_chopper_script_instance != null && m_owner == ePlayerTurn.computer)
            {
                TurnManager.m_chopper_script_instance.PropFlightPhase = Chopper.eFlightPhase.retreat;
            }
            m_hp = value >= 0 ? value : 0;
            if (m_hp == 0)
            {
                TurnManager.m_lost_player = m_owner;
                TurnManager.m_player_turn = ePlayerTurn.game_over;
            }
            m_hp_text.text = "HP: " + m_hp;
        }
    }

    private Text m_hp_text = null;
    public static int TranslateHitPart2Damage(string a_hit_gameobject_name)
    {
        switch (a_hit_gameobject_name)
        {
            case "front_wall":
            case "front_tower":
                {
                    return (int)eCastlePartDamage.front;
                }
            case "main_tower":
                {
                    return (int)eCastlePartDamage.main;
                }
            case "back_tower":
            case "back_wall":
                {
                    return (int)eCastlePartDamage.back;
                }

            default:
                {
                    return 0;
                }
        }
    }
    void Awake()
    {
        m_hp_text = GetComponentInChildren<Text>();
        m_hp_text.text = "HP: " + m_hp;
    }
}
