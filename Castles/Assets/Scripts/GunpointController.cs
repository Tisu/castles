﻿using UnityEngine;
using System.Collections;

public class GunpointController
{
 
    private GameObject m_gunpoint = null;
    private GameObject m_gunpoint_instance = null;
    public const float MAX_GUNPOINT_SCALE = 1.5f; //max gunpoint scale
    Color m_normal_color;

    bool m_red_color = false; //due to callling in update, make more efficient
    bool m_normal = true;

    public GunpointController(GameObject a_gunpoint)
    {
        m_gunpoint = a_gunpoint;
        m_normal_color = a_gunpoint.GetComponent<SpriteRenderer>().color;
    }
    public GameObject GunpointInstance()
    {
        if (m_gunpoint_instance == null)
        {
            return null;
        }
        return m_gunpoint_instance;
    }
    public void InstantiateGunpoint(Vector3 a_spawn_point, Quaternion a_transform_rotation)
    {
        m_gunpoint_instance = Object.Instantiate(m_gunpoint, a_spawn_point, a_transform_rotation) as GameObject;
        m_gunpoint_instance.transform.localScale = new Vector3(0.5f, 2f, 1f);
    }
    public void DestroyGunpoint()
    {
        if (m_gunpoint_instance != null)
        {
            Object.Destroy(m_gunpoint_instance);
            m_gunpoint_instance = null;
        }
    }
    public void RescaleGunpoint(float a_magnitude)
    {
        m_gunpoint_instance.transform.localScale = new Vector3(a_magnitude, m_gunpoint_instance.transform.localScale.y, m_gunpoint_instance.transform.localScale.z);
    }
    public void CantShoot()
    {
        if (!m_red_color && m_gunpoint_instance!=null)
        {
            m_red_color = true;
            m_normal = false;
            m_gunpoint_instance.GetComponent<SpriteRenderer>().color = Color.red;
        }
    }
    public void CanShoot()
    {
        if (!m_normal && m_gunpoint_instance!=null)
        {
            m_normal = true;
            m_red_color = false;
            m_gunpoint_instance.GetComponent<SpriteRenderer>().color = m_normal_color;
        }
    }
}
