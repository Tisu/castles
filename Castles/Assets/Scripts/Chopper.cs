﻿using UnityEngine;
using System.Collections;

public class Chopper : MonoBehaviour
{
    public enum eFlightPhase
    {
        ascend = 0,
        above_castle = 1,
        left_right = 2,
        retreat = 3,
    }

    [SerializeField]
    GameObject m_bomb;

    public Vector2 EnemyCastlePosition { private get; set; }

    public eFlightPhase PropFlightPhase
    {
        get
        {
            return m_flight_phase;
        }
        set
        {
            if (value == eFlightPhase.retreat)
            {
                Retreat();
            }
            m_flight_phase = value;
        }
    }
    eFlightPhase m_flight_phase = eFlightPhase.ascend;
    Vector2 m_movement_threshhold;
    const float m_height = 10f;
    bool m_left = false;
    bool m_right = false;
    Vector2 m_destination = Vector2.zero;
    int m_max_dropped_bombs = 0;
    int m_dropped_bombs = 0;
    float m_next_bomb_drop_time = 0f;
    float m_speed = 2f;
    void Awake()
    {
        m_movement_threshhold = new Vector2(Random.Range(3f, 7f), Random.Range(3f, 7f));
        m_max_dropped_bombs = Random.Range(1, 4);
        m_next_bomb_drop_time = Time.time + 100f;
        m_destination = (Vector2)transform.position + new Vector2(0, m_height);
    }
    void Update()
    {
        if (m_dropped_bombs == m_max_dropped_bombs)
        {
            Retreat();
        }
        else
        {
            if (m_flight_phase == eFlightPhase.ascend)
            {
                Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y, Camera.main.transform.position.z);
                Camera.main.orthographicSize = 7f;
            }
            if (CheckPostion())
            {
                if (m_left)
                {
                    m_destination = EnemyCastlePosition + new Vector2(m_movement_threshhold.x, m_height);
                    transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
                }
                else if (m_right)
                {
                    m_destination = EnemyCastlePosition + new Vector2(m_movement_threshhold.y, m_height);
                    transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);
                }
            }
        }
        DropBomb();
        transform.position = Vector2.MoveTowards(transform.position, m_destination, m_speed * Time.deltaTime);
    }

    private void Retreat()
    {
        int sign = TurnManager.m_single_mode_player == ePlayerTurn.first_player ? -1 : 1;
        m_destination = EnemyCastlePosition + new Vector2(20, 0) * sign;
        m_flight_phase = eFlightPhase.retreat;
        m_speed = 9f;
        TurnManager.m_chopper_spawned = false;
    }

    private void DropBomb()
    {
        if (m_next_bomb_drop_time < Time.time)
        {
            m_next_bomb_drop_time = Time.time + Random.Range(1f, 5f);
            Instantiate(m_bomb, transform.position - new Vector3(0, 0.5f, 0), Quaternion.Euler(new Vector3(0, 0, -90)));
            m_dropped_bombs++;
        }
    }
    /// <summary>
    /// return true if m_destination needs to be changed
    /// </summary>
    /// <returns></returns>
    private bool CheckPostion()
    {
        if (Vector2.Distance(transform.position, m_destination) < 0.15f)
        {
            switch (m_flight_phase)
            {
                case eFlightPhase.ascend:
                    m_flight_phase = eFlightPhase.above_castle;
                    m_destination = EnemyCastlePosition + new Vector2(0, m_height);
                    m_speed = 3.5f;                   
                    break;
                case eFlightPhase.above_castle:
                    {
                        m_flight_phase = eFlightPhase.left_right;
                        if (TurnManager.m_single_mode_player == ePlayerTurn.first_player)
                        {
                            TurnManager.m_player_turn = ePlayerTurn.first_player;
                            m_left = true;
                        }
                        else
                        {
                            TurnManager.m_player_turn = ePlayerTurn.second_player;
                            m_right = true;
                        }
                        break;
                    }
                case eFlightPhase.left_right:
                    m_left = !m_left;
                    m_right = !m_right;
                    if (m_dropped_bombs == 0)
                    {
                        m_next_bomb_drop_time = Time.time + Random.Range(1f, 3f);
                    }
                    break;
                case eFlightPhase.retreat:
                    {
                        Destroy(gameObject);
                        break;
                    }
                default:
                    break;
            }
            return true;
        }
        return false;
    }
    void OnTriggerEnter2D(Collider2D a_colider)
    {
        if (a_colider.name.Equals("bullet"))
        {
            Destroy(gameObject);
        }
    }
    void OnDestroy()
    {
        TurnManager.m_chopper_spawned = false;
        TurnManager.m_chopper_script_instance = null;
    }
}
