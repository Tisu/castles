﻿using UnityEngine;
using System.Collections;

public class WeaponMove : MonoBehaviour
{
    [SerializeField]
    private GameObject m_bullet = null;
    private Transform m_spawn_point = null;
    [SerializeField]
    private GameObject m_gunpoint = null;
#if UNITY_STANDALONE
    private const float FIRE_POWER = 60f;
    private float m_timer = 0;
    private Vector2 m_mouse_position = Vector2.zero;
#elif UNITY_ANDROID
    private Touch m_touch;
    private Vector2 m_touch_start_position = Vector2.zero;
    private Vector2 m_touch_insta_vector = Vector2.zero;
    private float m_touch_vector_magnitude = 0f;
    GunpointController m_gunpoint_controller = null;
    private const float DEVIDE_GUNPOINT_MAGNITUDE = 2f; //gunpoint size
#endif // platform
    public bool m_can_shot = false;
    public const float MAX_FIRE_POWER = 100f;

    private float m_rotation_z = 0f;
    private const float FIRE_POWER_RATIO = 60f;
    private float m_stop_rotation_z = 0f;
    private float m_bullet_power_private;
    const float SHOOT_TIME_THRESHOLD = 1f;
    float m_last_shoot_time = 0f;
    bool m_reset_gunpoint = false;   

    public float m_bullet_power
    {
        get
        {
            return m_bullet_power_private;
        }
        private set
        {
            m_bullet_power_private = value > MAX_FIRE_POWER ? MAX_FIRE_POWER : value;
        }
    }

    void Start()
    {
#if UNITY_ANDROID
        m_gunpoint_controller = new GunpointController(m_gunpoint);
#endif
        m_spawn_point = transform.Find("bullet_spawn_point");
    }

    void FixedUpdate()
    {
#if UNITY_STANDALONE

        m_mouse_position = (Vector2)Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
        m_mouse_position -= (Vector2)transform.position;
        m_rotation_z = Mathf.Atan2(m_mouse_position.y, m_mouse_position.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, m_rotation_z);
        if (Input.GetMouseButtonDown(0))
        {
            m_timer = Time.time;
        }
        if (Input.GetMouseButtonUp(0))
        {
            m_bullet_power = (Time.time - m_timer) * FIRE_POWER;
            Instantiate(m_bullet, m_spawn_point.transform.position, transform.rotation);
        }
#elif UNITY_ANDROID // other platforms
        if (m_gunpoint_controller.GunpointInstance() != null)
        {
            if (TurnManager.m_chopper_spawned)
            {
                VerifyGunPoint();
            }
            m_touch_insta_vector = Camera.main.ScreenToWorldPoint(m_touch.position);
            m_touch_vector_magnitude = (m_touch_insta_vector - m_touch_start_position).magnitude / DEVIDE_GUNPOINT_MAGNITUDE;
            m_touch_vector_magnitude = m_touch_vector_magnitude < GunpointController.MAX_GUNPOINT_SCALE ? m_touch_vector_magnitude : GunpointController.MAX_GUNPOINT_SCALE;
            m_gunpoint_controller.RescaleGunpoint(m_touch_vector_magnitude);
            m_rotation_z = Mathf.Atan2(m_touch_start_position.y - m_touch_insta_vector.y, m_touch_start_position.x - m_touch_insta_vector.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, m_rotation_z);
        }
#endif // platfrom specification
    }
#if UNITY_ANDROID
    void Update()
    {
        if (m_can_shot && Input.touchCount > 0)
        {
            if (Input.touchCount >= 2)
            {
                m_reset_gunpoint = true;
                m_gunpoint_controller.DestroyGunpoint();
                return;
            }
            m_touch = Input.touches[0];
            if (m_touch.phase == TouchPhase.Began && !m_reset_gunpoint)
            {
                m_gunpoint_controller.InstantiateGunpoint(m_spawn_point.transform.position, transform.rotation);
                m_gunpoint_controller.GunpointInstance().transform.parent = transform;
                m_touch_start_position = Camera.main.ScreenToWorldPoint(m_touch.position);
            }
            else if (m_touch.phase == TouchPhase.Ended && !m_reset_gunpoint)
            {
                m_bullet_power = m_touch_vector_magnitude * FIRE_POWER_RATIO < MAX_FIRE_POWER ? m_touch_vector_magnitude * FIRE_POWER_RATIO : MAX_FIRE_POWER;
                if (TurnManager.m_chopper_spawned && m_last_shoot_time + SHOOT_TIME_THRESHOLD < Time.time)
                {
                    m_last_shoot_time = Time.time;
                    Instantiate(m_bullet, m_spawn_point.transform.position, transform.rotation);
                }
                if (!TurnManager.m_chopper_spawned) // if normal turn
                {
                    Instantiate(m_bullet, m_spawn_point.transform.position, transform.rotation);
                }
                m_can_shot = false;
                m_gunpoint_controller.DestroyGunpoint();
            }
            else if (m_touch.phase == TouchPhase.Ended && m_reset_gunpoint)
            {
                m_reset_gunpoint = false;
            }
        }
    }

    private void VerifyGunPoint()
    {
        if (m_last_shoot_time + SHOOT_TIME_THRESHOLD > Time.time)
        {
            m_gunpoint_controller.CantShoot();
        }
        else
        {
            m_gunpoint_controller.CanShoot();
        }
    }
#endif
    void OnTriggerStay2D(Collider2D a_collider)
    {
        transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, transform.rotation.y, m_stop_rotation_z));
    }
    void OnTriggerEnter2D(Collider2D a_collider)
    {
        m_stop_rotation_z = transform.rotation.z;
    }
    public static void ValidatePower(ref float a_power)
    {
        a_power = a_power > MAX_FIRE_POWER ? MAX_FIRE_POWER : a_power;
    }
}
