﻿using UnityEngine;
using System.Collections.Generic;

public enum eWeaponType
{
    small = 0,
    medium = 1,
    large = 2
}
public struct WeaponInfo
{
    public float m_power;
    public float m_weapon_damage_rate;
    public eWeaponType m_weapon_type;
    public Vector3 m_position;
    public WeaponInfo(float a_power, float a_weapon_damage_rate, eWeaponType a_weapon_type, Vector3 a_position)
    {
        m_power = a_power;
        m_weapon_damage_rate = a_weapon_damage_rate;
        m_weapon_type = a_weapon_type;
        m_position = a_position;
    }
}
public class Weapon : WeaponMove, IWeaponInfo
{
    private eWeaponType m_weapon_type = eWeaponType.small;
    public eWeaponType m_weapon_typeP
    {
        get
        {
            return m_weapon_type;
        }
        set
        {
            m_weapon_type = value;
            GetComponent<SpriteRenderer>().sprite = m_weapon_sprites[WeaponType2SpriteIndex(m_weapon_typeP)];
        }
    }
    public static Dictionary<eWeaponType, float> m_damage_rate = new Dictionary<eWeaponType, float>() { { eWeaponType.small, 0.2f }, { eWeaponType.medium, 0.4f }, { eWeaponType.large, 1f } };
    private static Dictionary<eWeaponType, int> m_sprite_index = new Dictionary<eWeaponType, int>() { { eWeaponType.small, 0 }, { eWeaponType.medium, 1 }, { eWeaponType.large, 2 } }; //unnessecary can be refactor

    [SerializeField]
    Sprite[] m_weapon_sprites; //the smallest ->the largest
    private float WeaponDamageRate()
    {
        return m_damage_rate[m_weapon_type];
    }
    public static int WeaponType2SpriteIndex(eWeaponType a_weapon_type)
    {
        return m_sprite_index[a_weapon_type];
    }
    public WeaponInfo GetWeaponInfo()
    {
        return new WeaponInfo(m_bullet_power, WeaponDamageRate(), m_weapon_typeP, transform.position);
    }

}
