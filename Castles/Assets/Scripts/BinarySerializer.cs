﻿namespace TisuSerializer
{
    using UnityEngine;
    using System.Collections;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.IO;
    using System.Collections.Generic;

    public enum eFileStatus
    {
        save_successful,
        load_successful,
        save_failed,
        load_failed
    }
    /// <summary>
    /// Possible mistakes:
    /// - generic T class/struct isn't the same in load and save
    /// - You try to serialize nonserialize variable
    /// - check if you have [Serializable] attribute above T 
    /// 
    /// UNITY:
    /// - Neither Vetor3 nor Transfrom is NOT serializable
    /// </summary>
    public class BinarySerializer
    {
        private enum eFileError
        {
            file_doesnt_exist,
            argument_path_is_empty,
            member_path_is_empty,
            path_is_empty,
            file_ok,
            serialize_wasnt_successful,
            deserialized_wasnt_successful,
            deserialized_successful,
        }
        /// <summary>
        /// Last occurred error
        /// </summary>
#if DEBUG
        private eFileError m_last_file_error;
        /// <summary>
        /// Provides information about thrown exeptions
        /// </summary>
        public List<string> m_error { get; private set; }
#endif
        /// <summary>
        /// FULL_FILE_PATH assign in constructor
        /// </summary>
        private readonly string FULL_FILE_PATH;
        /// <summary>
        /// Default constructor  in save/load path is necessary!
        /// </summary>
        public BinarySerializer()
        {
            FULL_FILE_PATH = null;
#if DEBUG
            m_error = new List<string>();
#endif
        }
        /// <summary>
        /// Dont know if Application.persistentDataPath works on other apps than Unity. Use a_path for other.
        /// </summary>
        /// <param name="a_string"></param>
        public BinarySerializer(string a_string)
        {
            if (a_string.Contains("/"))
            {
                FULL_FILE_PATH = a_string;
            }
            else
            {
                FULL_FILE_PATH = Application.persistentDataPath + "/" + a_string;
            }
#if DEBUG
            m_error = new List<string>();
#endif
        }
        /// <summary>
        /// Saving [Serializable] data to a_path or FILE_NAME_WITH_EXTENSION
        /// </summary>
        /// <typeparam name="T">object type which contains data</typeparam>
        /// <param name="a_serialize_object">ref to the object from which data is saved </param>
        /// <param name="a_path">optional parameter path to save file WITH EXTENSION , when null constructor path is taken into account</param>
        /// <returns>eFileStatus.file_save_successful id successfull, otherwise see efilestatus</returns>
        public eFileStatus Save<T>(ref T a_serialize_object, string a_path = null)
        {
            BinaryFormatter binary_formater = new BinaryFormatter();
            FileStream file;

            if (string.IsNullOrEmpty(a_path))
            {
                if (string.IsNullOrEmpty(FULL_FILE_PATH))
                {
                    System.Diagnostics.Debug.Assert(false, "FILE_NAME_WITH_EXTENSION is empty");
#if DEBUG
                    m_last_file_error = eFileError.member_path_is_empty;
#endif
                    return eFileStatus.save_failed;
                }
                else
                {
                    file = File.Create(FULL_FILE_PATH);
                }
            }
            else
            {
                file = File.Create(a_path);
            }

            bool exeption_thrown = false;
            try
            {
                binary_formater.Serialize(file, a_serialize_object);
            }
            catch (System.Exception e)
            {
#if DEBUG
                m_error.Add(e.ToString());
#endif
                exeption_thrown = true;
            }
            finally
            {
                file.Close();
            }
            if (exeption_thrown)
            {
#if DEBUG
                m_last_file_error = eFileError.serialize_wasnt_successful;
#endif
                return eFileStatus.save_failed;
            }
            else
            {
                return eFileStatus.save_successful;
            }
        }
        /// <summary>
        /// Load data from a_path if not empty, then from FILE_NAME_WITH_EXTENSION to a_return_object
        /// </summary>
        /// <typeparam name="T">object type, data container</typeparam>
        /// <param name="a_return_object">ref to data container</param>
        /// <param name="a_path">optional parameter file path WITH EXTENSION</param>
        /// <returns>eFileStatus.deserialized_successfull if successfull</returns>
        public eFileStatus Load<T>(ref T a_return_object, string a_path = null)
        {
            if (!string.IsNullOrEmpty(a_path))
            {
                return LoadHelp<T>(ref a_return_object, a_path);
            }
            else if (!string.IsNullOrEmpty(FULL_FILE_PATH))
            {
                return LoadHelp<T>(ref a_return_object, FULL_FILE_PATH);
            }
            else
            {
                System.Diagnostics.Debug.Assert(false, "FILE_NAME_WITH_EXTENSION and a_path are empty");
#if DEBUG
                m_last_file_error = eFileError.path_is_empty;
#endif
                return eFileStatus.load_failed;
            }
        }
        private eFileStatus LoadHelp<T>(ref T a_load_object, string a_path)
        {
            if (File.Exists(a_path))
            {
                bool exeption_thrown = false;
                BinaryFormatter binary_formater = new BinaryFormatter();
                FileStream file = new FileStream(a_path, FileMode.Open);
                try
                {
                    a_load_object = (T)binary_formater.Deserialize(file);
                }
                catch (System.Exception e)
                {
#if DEBUG
                    m_error.Add(e.ToString());
#endif
                    exeption_thrown = true;
                }
                finally
                {
                    file.Close();
                }
                if (exeption_thrown)
                {
#if DEBUG
                    m_last_file_error = eFileError.deserialized_wasnt_successful;
#endif
                    return eFileStatus.load_failed;
                }
                else
                {
                    return eFileStatus.load_successful;
                }
            }
            else
            {
#if DEBUG
                m_last_file_error = eFileError.file_doesnt_exist;
#endif
                return eFileStatus.load_failed;
            }
        }
    }
}
