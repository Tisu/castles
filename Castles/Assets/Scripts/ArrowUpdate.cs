﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ArrowUpdate : MonoBehaviour
{
    public int m_castle_index = 2;
    ArrowSpawnPoint m_arrow_rotation = new ArrowSpawnPoint();
    Text m_distance_text = null;
    Canvas m_canvas = null;
    bool m_above = false;
    bool m_below = false;
    [SerializeField]
    Transform m_above_position = null;
    [SerializeField]
    Transform m_below_position = null;
    void Awake()
    {
        m_distance_text = GetComponentInChildren<Text>();
        m_canvas = GetComponentInChildren<Canvas>();
    }
    void FixedUpdate()
    {
        transform.rotation = m_arrow_rotation.CalculateArrowAngle(transform.position, m_castle_index);
        m_distance_text.text = m_arrow_rotation.Distance() + " m ";
        if (m_arrow_rotation.Distance() > Arrow.DISTANCE_DESTROY)
        {
            Destroy(gameObject);
            return;
        }
        else if (m_arrow_rotation.Distance() < Arrow.DISTANCE_THRESHOLD - 10f)
        {
            Destroy(gameObject);
        }
        if (!m_above && transform.eulerAngles.z >= 180)
        {
            m_above = true;
            m_below = false;
            m_canvas.transform.Rotate(new Vector3(0, 0, 1), 180);
            m_canvas.transform.position = m_below_position.position;
        }
        else if (!m_below && transform.eulerAngles.z < 180)
        {
            m_below = true;
            m_above = false;
            m_canvas.transform.position = m_below_position.position;
            m_canvas.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        }
    }
}
