﻿using UnityEngine;
using System.Collections.Generic;

public struct CannonCost
{
    public static Dictionary<eWeaponType, int> m_cannon_cost =
        new Dictionary<eWeaponType, int>() 
        {
            {eWeaponType.medium,7},
            {eWeaponType.large,10}
        };
}
