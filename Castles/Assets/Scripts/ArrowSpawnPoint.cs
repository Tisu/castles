﻿using UnityEngine;
using System.Collections;
using Tisu;
public class ArrowSpawnPoint
{
    Vector3 m_camera_castle = Vector3.zero;
    Vector3 m_spawn_point = Vector3.zero;

    public Vector3 CalculateArrowSpawnPoint(int a_castle_index , Vector3 a_transform_position,out Quaternion o_arrow_rotation_spawn)
    {
        m_camera_castle = TurnManager.CASTLES_MAIN_CAMERA_POSITION[a_castle_index] - a_transform_position;
        m_spawn_point = a_transform_position + m_camera_castle.normalized * 3;
        o_arrow_rotation_spawn = CalculateArrowAngle(a_transform_position, a_castle_index);
        return m_spawn_point;
    }
    public Quaternion CalculateArrowAngle(Vector3 a_transform_position,int a_castle_index)
    {
        m_camera_castle = TurnManager.CASTLES_MAIN_CAMERA_POSITION[a_castle_index] - a_transform_position;
        float angle = Mathf.Atan2(m_camera_castle.y, m_camera_castle.x) * Mathf.Rad2Deg - 90;
        return Quaternion.Euler(0, 0, angle);
    }
    public int Distance()
    {
        return (int)m_camera_castle.magnitude;
    }
}
