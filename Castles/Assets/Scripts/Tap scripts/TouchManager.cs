﻿using UnityEngine;
using System.Collections;

namespace Tisu.Touch
{
    public class TouchManager : MonoBehaviour
    {
        public LayerMask touchinputmask;

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
            foreach (var touch in Input.touches)
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                if (Physics.Raycast(ray, out hit, touchinputmask))
                {
                    GameObject reciepient = hit.transform.gameObject;
                    if (touch.phase == TouchPhase.Began)
                    {
                        reciepient.SendMessage("buttonTap", hit.point, SendMessageOptions.DontRequireReceiver);
                    }
                }

            }
        }
    }
}