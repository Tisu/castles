﻿using UnityEngine;
using System.Collections;

public class LoadLevelbyString : MonoBehaviour
{
    [SerializeField]
    private string LoadLevelString;
    [SerializeField]
    private eMode m_mode;
    private void buttonTap()
    {
        Application.LoadLevel(LoadLevelString);
        switch (m_mode)
        {
            case eMode.single:
                PlayerPrefs.SetInt("Mode", 1);
                break;
            case eMode.two_players:
                PlayerPrefs.SetInt("Mode", 2);
                break;
            default:
                break;
        }
    }
}
