﻿using UnityEngine;
using System.Collections;

public class ExitButton: MonoBehaviour 
{
    [SerializeField]
    GameObject m_explosion;
    private void buttonTap()
    {
        StartCoroutine(Explode());
    }
    IEnumerator Explode()
    {
        GameObject instance = Instantiate(m_explosion, transform.position - new Vector3(0,0,2), Quaternion.identity) as GameObject;
        ChangeExplosionScale(ref instance);
        yield return new WaitForSeconds(1f);
        Application.Quit();
    }
    private void ChangeExplosionScale(ref GameObject explosion)
    {
        explosion.transform.Find("FireBall").GetComponent<ParticleSystem>().startSize *= 20;
        explosion.transform.Find("Shockwave").GetComponent<ParticleSystem>().startSize *= 2;
        explosion.transform.Find("BaseSmoke").GetComponent<ParticleSystem>().startSize *= 3;
    }
}
