﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Arrow : MonoBehaviour
{
    [SerializeField]
    GameObject m_arrow = null;
    float m_distance = 0f;
    Text m_distance_text = null;
    ArrowSpawnPoint m_arrow_spawn_point = new ArrowSpawnPoint();
    bool m_arrow_spawned = false;
    GameObject m_arrow_instance = null;
    public const float DISTANCE_THRESHOLD = 50f;
    public const float DISTANCE_DESTROY = 150f;
    float m_time = 0f;
    void Awake()
    {
        m_time = Time.time;
        m_distance_text = m_arrow.GetComponentInChildren<Text>();
    }
    void FixedUpdate()
    {
        if (m_time + 1f > Time.time) //delay 1sec
        {
            return;
        }
        if (TurnManager.m_player_turn == ePlayerTurn.first_player)
        {
            if (m_arrow_spawned)
            {
                CheckDistance(1);
            }
            else
            {
                CalculateArrow(1);
            }
        }
        else if (TurnManager.m_player_turn == ePlayerTurn.second_player)
        {
            if (m_arrow_spawned)
            {
                CheckDistance(0);
            }
            else
            {
                CalculateArrow(0);
            }
        }
    }

    private void CheckDistance(int a_castle_index)
    {
        m_distance = (transform.position - TurnManager.CASTLES_MAIN_CAMERA_POSITION[a_castle_index]).magnitude;
        if (m_distance > DISTANCE_DESTROY)
        {
            Destroy(gameObject);
        }
    }
    void CalculateArrow(int a_castle_index /*0- castle1 1-castle2*/)
    {
        if (a_castle_index > TurnManager.CASTLES_MAIN_CAMERA_POSITION.Length - 1)
        {
            Debug.LogError("should never happened");
        }
        m_distance = (transform.position - TurnManager.CASTLES_MAIN_CAMERA_POSITION[a_castle_index]).magnitude;
        if (m_distance > DISTANCE_THRESHOLD)
        {
            m_arrow_spawned = true;
            Quaternion spawn_rotation = Quaternion.identity;
            Vector3 spawn_point = m_arrow_spawn_point.CalculateArrowSpawnPoint(a_castle_index, transform.position, out spawn_rotation);
            m_arrow_instance = Instantiate(m_arrow, spawn_point, spawn_rotation) as GameObject;
            m_arrow_instance.transform.parent = Camera.main.transform;
            m_arrow_instance.GetComponent<ArrowUpdate>().m_castle_index = a_castle_index;
        }
        else if (m_arrow_instance != null)
        {
            Destroy(m_arrow_instance);
            m_arrow_instance = null;
        }
    }
    void OnDestroy()
    {
        if (m_arrow_instance != null)
        {
            Destroy(m_arrow_instance);
        }
    }
}
