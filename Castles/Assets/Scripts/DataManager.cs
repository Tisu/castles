﻿using UnityEngine;
using System.Collections;
using TisuSerializer;

public struct SaveData
{
    public int m_current_wins;
    public eWeaponType m_current_weapon;
    public SaveData(int a_current_wins,eWeaponType a_weapon_type)
    {
        m_current_wins = a_current_wins;
        m_current_weapon = a_weapon_type;
    }
}
public class DataManager: MonoBehaviour 
{  
    const string FILE_NAME = "Data.dat";

    private static SaveData m_current_win_streak_private;
    BinarySerializer m_binary_serializer;
    public static  SaveData m_current_save_data 
    { 
        get
        {
            return m_current_win_streak_private;
        }
        set
        {
          
        }
    }
    public static int m_current_win_streak 
    { 
        get
        {
            return m_current_win_streak_private.m_current_wins;
        }
        set
        {
            if (m_current_win_streak_private.m_current_weapon != eWeaponType.large && m_current_win_streak_private.m_current_wins >= CannonCost.m_cannon_cost[m_current_win_streak_private.m_current_weapon])
            {
                m_current_win_streak_private.m_current_wins = 0;
                m_current_win_streak_private.m_current_weapon = m_current_win_streak_private.m_current_weapon +1;
                TurnManager.m_cannon_script_player1.m_weapon_typeP = m_current_win_streak_private.m_current_weapon;
                TurnManager.m_cannon_script_player2.m_weapon_typeP = m_current_win_streak_private.m_current_weapon;           
            }
            else
            {
                m_current_win_streak_private.m_current_wins = value;
            }
        }
    }
    void Awake()
    {
        m_current_win_streak_private = new SaveData();
        SaveData load_data = new SaveData();
        m_binary_serializer = new BinarySerializer(FILE_NAME);

        if (m_binary_serializer.Load(ref load_data) == eFileStatus.load_successful)
        {
            m_current_win_streak_private = load_data;
        }
        DontDestroyOnLoad(transform.gameObject);
    }
    void OnApplicationQuit()
    {
        m_binary_serializer.Save(ref m_current_win_streak_private);
    }

}
