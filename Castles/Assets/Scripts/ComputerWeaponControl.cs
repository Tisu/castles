﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public struct BulletDamageInfo
{
    public Vector3 m_hit_position_difference;
    public bool m_castle_hit;
    public BulletDamageInfo(Vector3 a_hit_position, bool a_castle_hit)
    {
        m_hit_position_difference = a_hit_position;
        m_castle_hit = a_castle_hit;
    }
}

public class ComputerWeaponControl : MonoBehaviour, IWeaponInfo
{
    [SerializeField]
    GameObject m_gunpoint = null;
    [SerializeField]
    GameObject m_bullet = null;
    [SerializeField]
    GameObject m_chopper = null;
    GunpointController m_gunpoint_controller = null;
    Transform m_gunpoint_spawn_transform = null;
    GameObject m_cannon = null;
    float m_power = 0;
    Weapon m_weapon_script;
    bool m_can_shoot = false;
    bool m_rotation_ended = false;
    float m_destination_angle = 0f;
    float m_angle_change = 0f;
    float m_angle_fake_move = 0f;
    float m_angle_fake_single_change = 0.2f;
    uint m_skip_update = 0;
    public static ComputerAgleCalculate m_computer_angle_calculate = new ComputerAgleCalculate();
  
    void Start()
    {
        if (!TurnManager.SINGLE_MODE)
        {
            return;
        }
        m_gunpoint_controller = new GunpointController(m_gunpoint);
        TurnManager.m_computer_control_script = this;
        if (TurnManager.m_player_turn == ePlayerTurn.first_player)
        {
            FindPositions("Castle 1"/*oponent*/, "Castle 2"/*computer*/, "cannon_player2"/*computer*/);
        }
        else
        {
            FindPositions("Castle 2", "Castle 1", "cannon_player1");
        }
    }
    void FixedUpdate()
    {
        if (++m_skip_update > 2)
        {
            if (m_skip_update > 3)
            {
                m_skip_update = 0;
            }
            return;
        }
        if (m_can_shoot)
        {
            if (Mathf.Abs(m_destination_angle - m_cannon.transform.eulerAngles.z) > 1f)
            {
                m_cannon.transform.rotation = Quaternion.AngleAxis(m_cannon.transform.eulerAngles.z + m_angle_change, Vector3.forward);
                m_rotation_ended = true;
            }
            else if (!m_rotation_ended)
            {

                m_angle_fake_move += m_angle_fake_single_change;
                m_cannon.transform.rotation = Quaternion.AngleAxis(m_angle_fake_move, Vector3.forward);
            }
            else
            {
                m_can_shoot = false;
                StartCoroutine(DelayShootLastStep());
            }
        }
    }
    public void Shoot()
    {
        if (Random.Range(0f, 1f) < 2f)
        {
            TurnManager.m_chopper_spawned = true;
            GameObject chopper_instance = Instantiate(m_chopper, new Vector3(m_computer_angle_calculate.m_computer_castle_position.x, m_computer_angle_calculate.m_computer_castle_position.y, -1), Quaternion.identity) as GameObject;
            chopper_instance.transform.localScale = new Vector3(TurnManager.m_single_mode_player == ePlayerTurn.first_player ? -1 : 1, chopper_instance.transform.localScale.x, chopper_instance.transform.localScale.z);//change chopper direction
            Chopper chopper_script =  chopper_instance.GetComponent<Chopper>();
            chopper_script.EnemyCastlePosition = m_computer_angle_calculate.m_enemy_castle_position;
            TurnManager.m_chopper_script_instance = chopper_script;
        }
        else
        {
            m_gunpoint_controller.InstantiateGunpoint(m_gunpoint_spawn_transform.position, m_cannon.transform.rotation);
            m_gunpoint_controller.GunpointInstance().transform.parent = m_cannon.transform;
            m_angle_fake_move = 0f;
            m_angle_fake_move += m_cannon.transform.eulerAngles.z;
            m_computer_angle_calculate.m_raycast_start = m_gunpoint_spawn_transform;
            ShootInfoShort shoot_info = m_computer_angle_calculate.AdjustAngle(m_cannon.transform.eulerAngles.z, out m_angle_change);
            m_power = shoot_info.m_power;
            m_destination_angle = shoot_info.m_angle;
            StartCoroutine(DelayShoot());
        }
    }

    private void FindPositions(string a_castle_name, string a_castle_computer_name, string a_cannon_name)
    {
        GameObject enemy_castle = GameObject.Find(a_castle_name);
        m_computer_angle_calculate.m_enemy_castle_position = enemy_castle.transform.Find("main_tower").transform.TransformPoint(Vector3.zero);

        GameObject computer_castle = GameObject.Find(a_castle_computer_name);
        m_cannon = computer_castle.transform.Find(a_cannon_name).Find("cannon").gameObject;
        m_computer_angle_calculate.m_computer_castle_position = m_cannon.transform.position;
        m_gunpoint_spawn_transform = m_cannon.transform.Find("bullet_spawn_point");
        m_weapon_script = m_cannon.GetComponentInChildren<Weapon>();
        if (TurnManager.SINGLE_MODE)
        {
            computer_castle.GetComponent<CastleDamage>().m_owner = ePlayerTurn.computer;
            enemy_castle.GetComponent<CastleDamage>().m_owner = ePlayerTurn.first_player;
        }
        if (m_computer_angle_calculate.m_cannon_reference_level == 0f)
        {
            m_computer_angle_calculate.m_cannon_reference_level = m_cannon.transform.eulerAngles.z;
        }

    }
    public WeaponInfo GetWeaponInfo()
    {
        return new WeaponInfo(m_power, Weapon.m_damage_rate[m_weapon_script.m_weapon_typeP], m_weapon_script.m_weapon_typeP, m_weapon_script.transform.position);
    }

    private IEnumerator GunpointDestroy()
    {
        yield return new WaitForSeconds(1f);
        m_gunpoint_controller.DestroyGunpoint();
    }
    private IEnumerator DelayShoot()
    {
        yield return new WaitForSeconds(Random.Range(1.5f, 2f));
        m_can_shoot = true;
    }
    IEnumerator DelayShootLastStep()
    {
        yield return new WaitForSeconds(Random.Range(0.5f, 1f));
        Instantiate(m_bullet, m_gunpoint_spawn_transform.position, m_cannon.transform.rotation);
        m_computer_angle_calculate.AddLastShootInfo(m_cannon.transform.eulerAngles.z, m_power);
        StartCoroutine("GunpointDestroy");
    }
}
