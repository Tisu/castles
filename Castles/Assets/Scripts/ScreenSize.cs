﻿using UnityEngine;
using System.Collections;

namespace Tisu
{
    public class ScreenSize
    {
        private static Vector2 m_screen_size = Vector2.zero;
        private static Vector2 m_screen_border_plus_value = Vector2.zero;
        private static Vector2 m_screen_border_minus_value = Vector2.zero;

        public static Vector2 m_screen_border_plus
        {
            get
            {
                UpdateScreenBordersPlus();
                return m_screen_border_plus_value;
            }
            set
            {
                UpdateScreenBordersPlus();
            }
        }
        public static Vector2 m_screen_border_minus
        {
            get
            {
                UpdateScreenBordersMinus();
                return m_screen_border_minus_value;
            }
            set
            {
                UpdateScreenBordersMinus();
            }
        }

        public static void UpdateScreenBordersPlus()
        {
            m_screen_size.x = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0))) * 0.5f; //Grab the world-space position values of the start and end positions of the screen, then calculate the distance between them and store it as half, since we only need half that value for distance away from the camera to the edge
            m_screen_size.y = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height))) * 0.5f;
            m_screen_border_plus_value = (Vector2)Camera.main.transform.position + m_screen_size;
        }
        public static void UpdateScreenBordersMinus()
        {
            m_screen_size.x = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0))) * 0.5f; //Grab the world-space position values of the start and end positions of the screen, then calculate the distance between them and store it as half, since we only need half that value for distance away from the camera to the edge
            m_screen_size.y = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height))) * 0.5f;
            m_screen_border_minus_value = (Vector2)Camera.main.transform.position - m_screen_size;
        }
    }
}
