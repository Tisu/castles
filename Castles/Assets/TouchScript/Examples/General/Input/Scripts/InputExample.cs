using TouchScript;
using UnityEngine;

public class InputExample : MonoBehaviour
{
    

    private void OnEnable()
    {
        if (TouchManager.Instance != null)
        {
            TouchManager.Instance.TouchesBegan += touchBeganHandler;
        }
    }

    private void OnDisable()
    {
        if (TouchManager.Instance != null)
        {
            TouchManager.Instance.TouchesBegan -= touchBeganHandler;
        }
    }

    
    private void touchBeganHandler(object sender, TouchEventArgs e)
    {
        foreach (var point in e.Touches)
        {
            Application.LoadLevel(2);

        }
    }
}